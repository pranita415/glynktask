package com.glynk.glynktask.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glynk.glynktask.R;

/**
 * Created by senthil on 17-Jan-18.
 */

public class Movies extends Fragment {

    TextView textview, textview1, textview2,textview3;
    public Movies() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.movie_fragment, container, false);

        textview = rootview.findViewById(R.id.textview);
        textview1 = rootview.findViewById(R.id.textview1);
        textview2 = rootview.findViewById(R.id.textview2);
        textview3 = rootview.findViewById(R.id.textview3);

        textview.setMovementMethod(LinkMovementMethod.getInstance());
        textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse("https://in.bookmyshow.com/bengaluru"));
                        startActivity(browserIntent);
                    }
                });

        textview1.setMovementMethod(LinkMovementMethod.getInstance());
        textview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("https://in.bookmyshow.com/bengaluru"));
                startActivity(browserIntent);
            }
        });

        textview2.setMovementMethod(LinkMovementMethod.getInstance());
        textview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("https://in.bookmyshow.com/bengaluru"));
                startActivity(browserIntent);
            }
        });

        textview3.setMovementMethod(LinkMovementMethod.getInstance());
        textview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("https://in.bookmyshow.com/bengaluru"));
                startActivity(browserIntent);
            }
        });

        return rootview;
    }
}
