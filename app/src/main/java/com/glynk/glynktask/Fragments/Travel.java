package com.glynk.glynktask.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glynk.glynktask.R;

/**
 * Created by senthil on 17-Jan-18.
 */

public class Travel extends Fragment
{
    public Travel() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview =  inflater.inflate(R.layout.travel_fragment, container, false);
        TextView read = rootview.findViewById(R.id.continue_to_read);

        read.setMovementMethod(LinkMovementMethod.getInstance());
        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("https://www.tripoto.com/trip/here-s-how-i-managed-to-take-12-trips-in-2017-with-a-9-to-5-job-5a43f6784e783"));
                startActivity(browserIntent);
            }
        });
        return rootview;
    }
}
