package com.glynk.glynktask.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;


import com.glynk.glynktask.MainActivity;
import com.glynk.glynktask.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by senthil on 17-Jan-18.
 */

public class Sports extends Fragment
{
    RecyclerView mRecyclerView;

    public Sports() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sports_fragment, container, false);
        //mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);

//        sports = new ArrayList<>();
//        adapter = new SportsAdapter(sports,getActivity());
//        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        mRecyclerView.setLayoutManager(horizontalLayoutManager);
//        mRecyclerView.setAdapter(adapter);
//        Data();

        return rootView;
    }
}
