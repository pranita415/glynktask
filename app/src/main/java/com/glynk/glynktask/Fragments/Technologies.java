package com.glynk.glynktask.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glynk.glynktask.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by senthil on 17-Jan-18.
 */

public class Technologies extends Fragment
{
    public Technologies() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.technologies_fragment, container, false);


        return rootView;
    }


}
