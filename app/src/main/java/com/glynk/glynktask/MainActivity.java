package com.glynk.glynktask;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.glynk.glynktask.Fragments.News;
import com.glynk.glynktask.Fragments.Movies;
import com.glynk.glynktask.Fragments.Sports;
import com.glynk.glynktask.Fragments.Technologies;
import com.glynk.glynktask.Fragments.Travel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    public ViewPagerAdapter adapter;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    Toolbar toolbar;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        tabLayout.setupWithViewPager(mViewPager);
        setupViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(5);
        tabLayout.setOnTabSelectedListener(MainActivity.this);
        context = this;

        setSupportActionBar(toolbar);
        setTitle("Glynk");
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        int tabcount;

        public ViewPagerAdapter(FragmentManager manager, int tabcount) {
            super(manager);
            this.tabcount = tabcount;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            System.out.println("size" + mFragmentList.size() + "// " + tabcount);
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        adapter.addFragment(new Travel(), "Travel Blog");
        adapter.addFragment(new Movies(), "Movies");
        adapter.addFragment(new Technologies(), "Tech");
        adapter.addFragment(new News(), "News");
        adapter.addFragment(new Sports(), "Sports");
        viewPager.setAdapter(adapter);
    }
}

